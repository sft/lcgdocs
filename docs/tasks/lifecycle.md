# LCG Platform and Stack Lifecycle and Sun-setting


To create a new stack or platform or both one has to

* create a new toolchain file in `lcgcmake/cmake/toolchains/heptools-NEWNAME.cmake`,
* add the toolchain to the [general matrix](https://lcgapp-services.cern.ch/spi-jenkins/user/sailer/my-views/view/Favorites/job/lcg_nightly_general_matrix/) with the desired platforms, selected in the combinations filter under advanced,
* add the toolchain to the [lcg_clean_cvmfs_nightlies](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_clean_cvmfs_nightlies/) job, so that nightlies are removed regularly,
* add the toolchain to the [lcginfo_nightlies](https://lcgapp-services.cern.ch/spi-jenkins/job/lcginfo/job/lcginfo_nightlies/) job, to publish information on lcginfo.cern.ch
* Add links on `/cvmfs/sft.cern.ch/lcg/views/<TOOLCHAIN>/` to `/cvmfs/sft-nightlies.cern.ch/lcg/views/<TOOLCHAIN>/[Mon,Tue,Wed,Thu,Fri,Sat,latest]`. This is used by some tests and also for easy discovery of the stacks

If a new platform (architecture-operating_system-compiler-mode) is added

* add the platform to the [lcg_latest_general_matrix](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_latest_general_matrix/) job, to install packages slightly more permanently than into the nightlies area.


## Cleanup jobs

* Cleaning nightly views, or other folders from cvmfs-sft-nightlies: [lcg_clean_cvmfs_nightly_views](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_clean_cvmfs_nightly_views/)

## Manual Tasks

* If a stack and platform combination is no longer needed, the LCG_*.txt file in the folder `/eos/project/l/lcg/www/lcgpackages/tarFiles/latest` has to be removed, so that the [lcg_purge_binary_repository](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_purge_binary_repository/) job can clean any remaining latest files. 
* If a platform is completely removed, make sure the [lcg_latest_general_matrix](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_latest_general_matrix/) removed them also from `/cvmfs/sft-nightlies.cern.ch/lcg/latest`. This job requires an empty "summary" file to exist under `/eos/project/l/lcg/www/lcgpackages/tarFiles/latest`, which can be crated with the [lcg_summaries_creation](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_summaries_creation/) job.
* If a stack was removed, the respective combination should be removed from the "Configuration Matrix" of [lcg_clean_cvmfs_nightlies](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_clean_cvmfs_nightlies/) a week after the stack and platform combination was removed.
* The [lcg_clean_cvmfs_nightly_views](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_clean_cvmfs_nightly_views/) job can be used to cleanup views that are no longer needed. 
