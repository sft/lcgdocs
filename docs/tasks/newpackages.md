# Adding new system packages

Usually, the obvious way to add packages to the system layer is to ask [Andrea Valassi](mailto:Andrea.Valassi@cern.ch) to add them to the [HEP_OSlibs](https://gitlab.cern.ch/linuxsupport/rpms/HEP_OSlibs). Since the HEP_OSlibs are a bit too big for some use cases and a quick solution is needed, here's an explanation how to add packages to the Docker images (used for nightly builds) or the puppetized VMs (used for releases).

## Docker

In Docker, there's a `Dockerfile` per operating system. Since *CentOS 7/8/9* are provided by IT, we extend the functionality by adding additional packages and creating the necessary folders. For *Ubuntu* and *Fedora* we customize the entire system to our needs.

As far as naming for docker images goes:

* CentOS 7 is called `centos7` (or `cc7` as legacy image)
	* Based on an image provided by CERN IT
	* Uses `HEP_OSlibs`
* Fedora 28 is called `fedora28`
	* No `HEP_OSlibs` (lightweight image)
* Fedora 29 is called `fedora29`
	* No `HEP_OSlibs` (lightweight image)
* Fedora 30 is called `fedora30`
	* No `HEP_OSlibs` (lightweight image)
* Ubuntu 18.04 is called `ubuntu18`
	* LTS release (long term support)
	* No `HEP_OSlibs` (lightweight image)
* Ubuntu 19.04 is called `ubuntu19`, a regular release
	* Regular Ubuntu release (short support)
	* No `HEP_OSlibs` (lightweight image)

In the [Container Registry](https://gitlab.cern.ch/sft/docker/container_registry) you can find each image with two different name:

* The old approach is to keep all operating systems under the same name and use the *tag* as a means to tell which OS the image refers to, e.g. `gitlab-registry.cern.ch/sft/docker:lcg-cc7`
* As a new (parallel to the old) approach there's a naming scheme that uses the *tag* only for versioning and not to specify the OS: e.g. `gitlab-registry.cern.ch/sft/docker/centos7:latest`

The new naming approach drops the prefix `lcg-` for all images and renames `cc7` to `centos7`. Currently, it is not yet used in our scripts. Still this way is more concise with the Docker way of naming and tagging images.

### Docker: Preconditions

* You need to have at least the role `Developer` in the project `sft/docker`: [https://gitlab.cern.ch/sft/docker/project_members](https://gitlab.cern.ch/sft/docker/project_members)

### Docker: Step-by-step

1. Find out the correct name of the package or library you want to add for each platform. Usually there are two names: One for Debian/Ubuntu and one for RedHat/SLC/CentOS/Fedora.
2. Add the package name to the file `packages.txt`, they are in the respective subfolder in the `builder` directory.
3. Add, commit and push your changes to the git repository with a comment that explains why the new package is needed.
4. Run the Gitlab CI job(s) that correpond to the container that was changed by clicking on the "Play" button of the respective `bk` job.

## Puppet

Puppet is a configuration management tool that is used by CERN to manage the SLC 6 and CentOS 7 VMs.
Ubuntu, Fedora and other Linux flavors are not supported by CERN IT.
Each Puppet hostgroup has a GitLab repository in the `ai` group.
Our repository is called [it-puppet-hostgroup-lcgapp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lcgapp).

In Puppet, you describe the desired state of your target system (e.g. a CentOS 7 VM).
For example you don't tell Puppet *"Install package XY!"* but instead *"Ensure that package XY is installed!"*

Puppet runs once per hour on each VM and verifies that the target system is in the described state.
If not, it takes the necessary steps to reach this state, e.g. install a missing package.

You can lookup the current state of all puppetized VMs in a web application called *Foreman*.
It runs at CERN on a server called [judy](https://judy.cern.ch) (requires CERN certificates for TLS)

The most relevant branches in the git repository are:

* `master` refers to the environment *Production*
* `qa` refers to the environment *QA*

We use both environments for our build nodes.
The recommended environment is *Production*.

A manual reload of the latest configuration on a puppetized VM can be forced - for impatient developers :)

```bash
puppet agent -tv
```

### Puppet: Preconditions

* You need to be in the following eGroups:
	* [VOBox-Responsible-SFT](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=189044)
	* [ai-admins](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=326041)
* You are automatically assigend as member of the GitLab project [it-puppet-hostgroup-lcgapp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lcgapp/project_members) as a *Developer* if you are in the eGroup ai-admins.

### Puppet: Step-by-step

1. Find out which hostgroup and which environment you want to modify, e.g. via [Foreman](https://judy.cern.ch/hostgroups)
2. Clone the git repository
	* Checkout the `qa` branch if necessary (if you want to change QA instead of Production)
3. Find out the correct name of the missing package for CentOS/RedHat releases. There's a difference in naming across different Linux flavors.
4. Find your hostgroup's specification file, e.g. `code/manifests/x86_64/build/cc7/docker.pp` for the hostgroup `lcgapp/x86_64/build/cc7/docker`
5. Use the Puppet [resource type package](https://puppet.com/docs/puppet/5.5/types/package.html) to ensure the presence of your desired package `packageXY`: See instructions below
6. Add, commit and push your changes with a comment that explains why the new package has been added
7. Repeat steps 3 to 5 for another branch if your target machines are split across multiple environments, e.g. first add the package in the `master` branch and then in the `qa` branch
8. Wait for an hour or force a reload with the command mentioned above.
9. Check the report for an effected node in Foreman to make sure that no errors occurred

#### Puppet: Ensure presence of `packageXY`

```puppet
package{ "packageXY" :
   ensure => present
}
```

You can also pass a list of packages:

```puppet
package{ ['packageA', 'packageB', 'packageC'] :
   ensure => present
}
```

