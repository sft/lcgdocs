# Publish LCG sources
Sources of a given release should be made available to experiments on request. This can help them debug issues they may encounter. It is desirable to have these sources in an unarchived state on CVMFS, from where debuggers can use them directly.

**The sources should:**

1.  Be the sources used for each particular package of a particular release. 
2.  Have patches applied as done by LCGCMake. 
3.  Be accessible at `/cvmfs/sft.cern.ch/lcg/sources/`.

**Two Jenkins jobs exist to automate this process:**

*  [lcg\_generate\_buildinfo](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_generate_buildinfo/) runs the LCGCMake setup in in order to generate a list of packages and their corresponding EOS URL for a given release. This list is uploaded to `/eos/project-l/lcg/www/lcgpackages/source_buildinfo/`.
*  [lcg\_publish\_sources](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_publish_sources/) downloads first this list, then every source on that list and patches it. The result is published on CVMFS. 
