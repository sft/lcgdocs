# CDash Service

The CDash service for the EP-SFT group runs on: [https://cdash.cern.ch](https://cdash.cern.ch).
Its current version is **2.6.0** and it runs in a dedicated container on the host `lcgapp-cdash.cern.ch`.
The access to the host is granted by the [`vobox-responsible-sft` e-group](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=189044).

## Database

The current database is provided by the [DBoD Service](https://cern.ch/DBOnDemand) (Database On Demand):

- **Type:** MySQL
- **Host:** `dbod-cdash-production.cern.ch`
- **Login:** `admin`
- **Port:** `5500`
- **Database:** `cdash`

## Docker configuration

The base docker container has been taken from the official [CDash repo](https://github.com/Kitware/CDash).
It needs some adjustment for the CERN environment.

## CERN specific configuration

CERN specific changes to the docker container are

- removal of the internal MySQL server
- a new apache config to allow both http and https
- enabling of mod_ssl and mod_rewrite
- addition of machine certificates
- database settings and CERN email addresses

These files can be found on a [fork](https://gitlab.cern.ch/hegner/CDash) of CDash.

## Bootstrapping

If there is ever need for bootstrapping the database again, the current production 
version of CDash 2.6.0 has two minor issues, which need fixing in case of setting 
up a database from scratch. One is a missing quotation in `sql/mysql/cdash.sql`:

    1297c1297
    <   KEY `system` (osid)
    ---
    >   KEY system (osid)


The other minor issue is the fact that the first project needs fixing by hand in the DB.
First connect to it:

    mysql -h dbod-cdash-production.cern.ch -P 5500 -u admin -p

And then within the DB session

    use cdash;
    update project set name="LCGSoft" where name='testing’;
    insert into project (name) values ("testing");
  
In addition, the CERN version of MySQL is more strict than the queries used by CDash.
This mainly affects the display of test results. To fix this, one has to change the
[ONLY_FULL_GROUP_BY](https://dev.mysql.com/doc/refman/8.0/en/group-by-handling.html)
setting within the MySQL console:

    SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));


## Certificate renewal

- Create a [new Grid Host Certificate](https://ca.cern.ch/ca/host/Request.aspx?template=ee2host)
  + Click on Automatic certificate generation.
  + Define the name of the host, currently cdash is running on: phsft-web1.cern.ch.
  + Download the certificate (.p12 file), keep safe this certificate.

- Copy the .p12 file to the cdash server.
- Generate a .crt file (if the certificate was generated without password, just press enter):

  ```
  openssl pkcs12 -in <certificate_name>.p12 -clcerts -nokeys -out <certificate_name>.crt
  ```

- Generate a .pem file:

  ```
  openssl pkcs12 -in <certificate_name>.p12 -nocerts -nodes -out <certificate_name>-key.pem
  ```
- Change the permissions of the latter:

  ```
  chmod 0600  <certificate_name>-key.pem
  ```

- Modify the apache configuration:

  + In the `<VirtualHost *:443>` section modify the following options with the proper paths:

    ```
    SSLCertificateFile /path/to/<certificate_name>.crt
    SSLCertificateKeyFile /path/to/<certificate_name>-key.pem
    ```
