# Acquiring `sftnight` credentials

The service account `sftnight` is used by the EP-SFT group for many services. For example, the `Jenkins` jobs
run always under `sftnight`. Usually the members of the EP-SFT group or their external partners 
do not need to acquire credentials for `sftnight`. There are a few cases when this is necessary. For such cases,
procedure to get credentials for `sftnight` is described in this page.

## The `sftnight-access` e-group
Users enabled to become `sftnight`:

1. Have a CERN account (can login on lxplus.cern.ch)
2. Are the members of the [CERN e-group](https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=sftnight-access)


Users can issue a request for e-group membership on the e-group site. The request is accorded on case-by-case base, and reguraly reviewed.

## Acquiring `sftnight` credentials
The process to acquire `sftnight` credentials requires:

1. To login on lxplus with the user's credentials
2. Execute `~sftnight/public/lxplus` to become `sftnight`

Example:
```
$ ssh lxplus.cern.ch
...
lxplus793:~ $ whoami
ganis
lxplus793:~ $ ~sftnight/public/lxplus
...
lxplus751:~ $ whoami
sftnight
```
