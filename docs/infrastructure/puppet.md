# Puppet configuration

The current puppet configuration is done in flexible way, that can handle sudden incompatibility, use of platform
specific software and different needs depending on the project and not breaking the other configurations.

## Construction of the puppet configuration

The general construction of the puppet configuration is divided in two main branches: *code* (manifest files, templates, text files) and *data* (hiera data). For more information about the go to [CERN Configuration Management System User Guide](https://configdocs.web.cern.ch/configdocs/)

### Manifest files

The current configuration is strongly depending on hierarchical property of puppet configuration.
If we would like to apply this configuration, we would use a path to it starting from lcgapp:
`lcgapp/<architecture>/<purpose>/<platform>/<specialisation>`.

Example:
lcgapp/x86_64/build/cc7/docker
The hierarchical property in puppet will compile the configuration by applying at every step the configuration, which
means: init.pp, x86_64.pp, build.pp, cc7.pp and docker.pp. The conflicts will be solved by choosing the later option
(override the previous). 

- `init`: General configuration used across all the machines - not specific arch and system - e.g. credentials
- `architecture`: processor architecture specific configuration (i386, x86_64, [arm]) - e.g. include module compatible only for x86_64, like eosclient
- `purpose`: purpose of the machine (build, service, project) - general specialisation of the machine - e.g. install build tools
- `platform`: define system specific configuration (cc7, slc6, [ubuntu]) - system configuration - e.g. install system specific software, like HEPOS_libs
- `specialisation`: define system specialisation (docker) - subspecialisation - e.g. docker inside build machine

## The way of handling platform incompatibility

Suppose we have:
init.pp
- x86_64.pp
- i386.pp

If the eosclient does no longer support i368 platform (which it really doesnt support anymore), we move the line one
level higher to platform which does support - from init to  x86_64. This ensures that the platform x86_64 does not change its
final configuration, although the underling files changed. For the i386 platform we have to figure out an other
solution, but it does not affect other platforms.

## Modules
Some pieces of code can be reused by different hostgroups by using custom configuration or for different
branches/projects that use the same technology, e.g. docker, mounting volume

#### Existing modules:

| Modules  | Description |
| -------- | ----------- |
| ccache   | Install ccache on the machine |
| docker   | Install and configure docker on the machine |
| credential | Put keytab file in the required directory |
| ec       | Create /ec dir and put credentials |
| pkg      | Contains classes with groups of common packages: basic, build, dev, ...|
| cvmfs    | Mounts list of cvmfs repositories |
| mount    | Mounts device to mountpoint (creates fs, create mount dir, mount, set permission) |
| lcgtools | Creates directory /lcg with lcgtools and common scripts |

## Troubleshooting

### CVMFS cannot mount 
`change from absent to directory failed: Could not set 'directory' on ensure: Permission denied @dir_s_mkdir`

Probable problem is mounting cvmfs in two ways at the same time. E.g. via autofs and fstab.
Solution for the fstab - disabling autofs:

- CC7  - systemctl disable autofs; systemctl stop autofs

### mount: /dev/vdb already mounted

If you are migrating from old configuration, it already is mounted in /mnt directory and tries to mount to /mnt/build
To fix this, you need to remove line with /mnt or run `sed -i '/mnt\t/d' /etc/fstab`

### Name or service not known 
```
$> puppet agent -tv
Warning: Unable to fetch my node definition, but the agent run will continue:
Warning: getaddrinfo: Name or service not known
Info: Retrieving pluginfacts
Error: /File[/opt/puppetlabs/puppet/cache/facts.d]: Failed to generate additional resources using 'eval_generate':
getaddrinfo: Name or service not known
Error: /File[/opt/puppetlabs/puppet/cache/facts.d]: Could not evaluate: Could not retrieve file metadata for
puppet:///pluginfacts: getaddrinfo: Name or service not known
Info: Retrieving plugin
Error: /File[/opt/puppetlabs/puppet/cache/lib]: Failed to generate additional resources using 'eval_generate':
getaddrinfo: Name or service not known
Error: /File[/opt/puppetlabs/puppet/cache/lib]: Could not evaluate: Could not retrieve file metadata for
puppet:///plugins: getaddrinfo: Name or service not known
Info: Loading facts
Error: Could not retrieve catalog from remote server: getaddrinfo: Name or service not known
Warning: Not using cache on failed catalog
Error: Could not retrieve catalog; skipping run
Error: Could not send report: getaddrinfo: Name or service not known
```

Cause: dhcpclient did not fill the */etc/resolv.conf* file (DNS server address)

To solve this problem copy /etc/resolv.conf from an other machine.

