# Create an account for an existing CERN user

To add a new CERN AFS user account to a computer you need to use the following command:

On CentOS7 you can use
```
useraddcern <cern_username_account>
```
On Later operating systems, AFS is no longer set as the default home directory for users. To preserve the AFS home directory you have to add the `--directory` flag for the `addusercern` call.

Prerequisites:

  - A CERN AFS account present/visible in the LDAP system (to look for an existing account: [phonebook](https://phonebook.cern.ch/phonebook/#))
  - Root privileges

