# SPI Shift: Checklist

1. Register to the **e-group** [project-lcg-spi-ci-notifications](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10313663) to get the mails from Jenkins.
2. Check the [nightly build report](https://lcgdocs.web.cern.ch/lcgdocs/report/) for new tickets (button on the top left) and make sure their are assigned (ask on Mattermost if unsure). The same action can be perfomed diectly on the [SPI JIRA main page](https://sft.its.cern.ch/jira/issues/?filter=-4&jql=project%20%3D%20SPI%20AND%20status%20in%20%28Open%2C%20%22In%20Progress%22%2C%20Reopened%29%20order%20by%20created%20DESC) if preferred: make sure they are assigned and followed
3. Check status of builds in the [Nightly Reporter](https://lcgdocs.web.cern.ch/lcgdocs/report/) and in [CDash](http://lcgapp-services.cern.ch/cdash/index.php?project=LCGSoft). Results are expected for the platforms defined in Jenkins
    - Nightlies `dev4`
    - Nightlies `dev3`
4. Investigate in [Jenkins](https://lcgapp-services.cern.ch/spi-jenkins/view/LCG%20Nightlies/) reasons for failures and/or non appearance in [CDash](http://cdash.cern.ch/index.php?project=LCGSoft)
    - Try to collect as much as possible information about the problem (build machine, type of failure, etc)
    - If the solution is not obvious fill a ticket in [JIRA SPI](https://sft.its.cern.ch/jira/projects/SPI/issues) under the component **LCG CI Issues**
5. Check other anomalies in Jenkins
    - Jobs taking too much time
    - Also for other projects;
    - Slowness, ...
    - Failed Jobs
    - Pending Jobs


When a problem is identified and the solution is not obvious:

1. Try to collect as much as possible information about the problem (build machine, type of failure, etc)
2. Fill a ticket in [JIRA SPI](https://sft.its.cern.ch/jira/projects/SPI/issues) under the component **LCG CI Issues**

