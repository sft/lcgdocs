# Java

For java we use the OpenJDK. Except that there are about 20 builds of OpenJDK, so we have to pick one and maybe stick
with it.

We are going to use the "Adoptium" builds of OpenJDK:

* OpenJDK.org for [java11](https://wiki.openjdk.org/display/JDKUpdates/JDK11u)
* OpenJDK.org for [java17](https://wiki.openjdk.org/display/JDKUpdates/JDK+17u)
