# How to build and install a LCG release for MacOS
The installation of MacOS binary packages in CVMFS requires to run MacOS programs for fixing the relocation of binary objects (libraries and executables) and this can not be done in the publisher nodes, which is centos7 based. This is the reason why the build/installation process is done 3 steps: first build binary packages in a build node and copy binary tarfiles to the binary repository, then a first installation and relocation using a MacOS node; and finally copying file tree to CVMFS in the publisher node. For Linux platforms only two steps are required.

## Build the release on a MacOS mode and upload binary tarfiles to EOS
First we need to produce the binary tarfiles on a Mac and upload them in the `release` area in EOS. We need to disable the `latest` area to make sure that we do not use any cached previously binary tarfile.

1. Clone the lcgcmake repository and add the `lcgcmake/bin` into the PATH:
    ```
    git clone https://gitlab.cern.ch/sft/lcgcmake.git
    git checkout LCG_<lcgversion>
    export PATH=$PWD/lcgcmake/bin:$PATH
    ```

2. Configure a using the required `toolchain`, build all packages, and upload the binaries to EOS
    ```
    mkdir build; cd build
    lcgcmake config -v <lcgversion> --with-tarfiles -p <local-prefix> \
                    -o LCG_ADDITIONAL_REPOS='' CMAKE_C_COMPILER=/usr/bin/clang CMAKE_CXX_COMPILER=/usr/bin/clang++
    lcgcmake install all
    ...
    lcgcmake upload tarfiles --release
    ```

## Do a second installation using the uploaded tarfiles into a `local` /cvmfs

3. Install the uploaded binaries in a `/cvmfs` volume [aka `/Users/Shared/cvmfs`] prepared in the local Mac. For this we need to unmount all the CVMFS volumes first. We can use node `macphsft21.cern.ch`.
    ```
    sudo umount /Users/Shared/cvmfs/sft.cern.ch
    sudo chown sftnight /Users/Shared/cvmfs/sft.cern.ch

    ...

    mkdir build;cd build
    lcgcmake config --version LCG_<lcgversion> \
                    --prefix /Users/Shared/cvmfs/sft.cern.ch/lcg/releases \
                    --toolchain ../lcgcmake/cmake/toolchain/heptools-<lcgversion>.cmake
    lcgcmake install all
    ```

## Copy installation to CVMFS using the publisher node (cvmfs-sft.cern.ch)

5. Reserve the [cvmfs-sft](https://lcgapp-services.cern.ch/spi-jenkins/computer/cvmfs-sft/) node in Jenkins by putting it temporary off-line.

6. Install in CVMFS by login to the installation node
    ```
    ssh sftnight@cvmfs-sft.cern.ch
    sudo -i -u cvsft
    cvmfs_server transaction sft.cern.ch

    cvmfs_rsync -av username@node:/Users/Shared/cvmfs/sft.cern.ch/lcg/releases /cvmfs/sft.cern.ch/lcg
    cvmfs_rsync -av username@node:/Users/Shared/cvmfs/sft.cern.ch/lcg/views /cvmfs/sft.cern.ch/lcg

    cvmfs_server publish
    exit
    exit
    ```
7. Don't forget to bring [cvmfs-sft](https://lcgapp-services.cern.ch/spi-jenkins/computer/cvmfs-sft/) back on-line in Jenkins

## Restore CVMFS in the temporary installation node
9. Restore the CVMFS mount points in 
    ```
    sudo mount -t cvmfs sft.cern.ch /Users/Shared/cvmfs/sft.cern.ch
    ```


