# LCG PIP Mirror
Some packages (e.g. llvmlite) are too complicated to build them from source. Instead, we use the "wheels" provided by the package maintainers.


## Procedure:
Run the [lcg-pip-mirror](https://lcgapp-services.cern.ch/spi-jenkins/view/LCG%20Externals/job/lcg-pip-mirror) jenkins
job, specifying the package and versions and the desired Python version and platforms.

### Platforms to place in our mirror

The list of Python platforms (not to be confused with LCG platforms) are in the parameters `LINUX_TXT` and
`MACOSX_TXT`. The former doesn't change often (the last one - `manylinux2014` - was accepted on July 31, 2019), the
latter should be updated once we adopt a new version of MacOS.
With the EOL of CentOS7 we can start using manylinx_2_28 for EL9 based operating systems, and for the Python 3.11 and
3.12 versions exclusively.

## Gitlab

The [gitlab repository](https://gitlab.cern.ch/sft/lcg-pip-mirror) contains the pipeline to upload packages to the mirror.


## Nexus Pypi

The PYthonPackageIndex is provided by the Nexus package repository. Found under
https://lcgpipmirror.web.cern.ch/. Access to the WebUI is quite limited, but in principle uploading can be done by the
[lcg-pip-mirror](https://lcgapp-services.cern.ch/spi-jenkins/view/LCG%20Externals/job/lcg-pip-mirror) Jenkins job, and
anonymous download is available inside the CERN network.

### Details:

The lcgpipmirror is hosted in the cern
[Webservices](https://webservices-portal.web.cern.ch/app-catalogue/lcg-pip-mirror). Access to the WebUI can be granted
by the Administrators of that webservice, and should be available to project-lcg-spi-internal.


## Rationale
Some packages (e.g. llvmlite) are too complicated to build them from source. Instead, we use "wheels" (zip files containing pre-built binaries). 
Problem: the naming of these wheels is a bit tricky, e.g.:
```cmake
if(Python_native_version VERSION_LESS 3)
  if(${LCG_OS} MATCHES mac )
    set(llvmlite_flav cp27-cp27m-macosx_10_9)
  else()
    set(llvmlite_flav cp27-cp27mu-manylinux1)
  endif()
elseif(Python_native_version VERSION_LESS 3.7)
  if(${LCG_OS} MATCHES mac )
    set(llvmlite_flav cp36-cp36m-macosx_10_9)
  else()
    set(llvmlite_flav cp36-cp36m-manylinux1)
  endif()
elseif(Python_native_version VERSION_LESS 3.8)
  if(${LCG_OS} MATCHES mac )
    set(llvmlite_flav cp37-cp37m-macosx_10_9)
  else()
    set(llvmlite_flav cp37-cp37m-manylinux1)
  endif()
elseif(Python_native_version VERSION_LESS 3.9)
  if(${LCG_OS} MATCHES mac )
    set(llvmlite_flav cp38-cp38-macosx_10_9)
  else()
    set(llvmlite_flav cp38-cp38-manylinux1)
  endif()
elseif(Python_native_version VERSION_LESS 3.10)
  if(${LCG_OS} MATCHES mac )
    set(llvmlite_flav cp39-cp39-macosx_10_9)
  else()
    set(llvmlite_flav cp39-cp39-manylinux1)
  endif()
endif()

if(llvmlite_native_version VERSION_GREATER_EQUAL 0.34.0)
  string(REPLACE manylinux1 manylinux2010 llvmlite_flav ${llvmlite_flav})
endif()

# ...
    INSTALL_COMMAND ${PYTHON} ${pip_home}/bin/pip install ${PySetupOptions} ${GenURL}/llvmlite-${llvmlite_native_version}-${llvmlite_flav}_x86_64.whl
```

Solution: let `pip` figure out the exact wheel name from python version, package name and version.

Added benefit: if there is a new dependency (not provided by the same mirror or not built by ourselves), the installation will fail.
