# NXCALS

NXCALS is a new logging system from BE-CO-DS that will soon replace the old CALS system.
It uses big data frameworks such as Spark and Hadoop.
The maintenance and communication is done by the IT-DB-SAS team.

* NXCALS is used in production -> Focus on stability
* Used mostly on [SWAN](https://swan.cern.ch) but also on lxplus
* Only Python 3, no Python 2 required
* Uses currently the full stack, although probably not all packages are required by the NXCALS
* Generators, COOL, CORAL and Garfield has now been removed from the layer.
* Additionally, newer NXCALS builds (from LCG_100_nxcals and up) contains two NXCALS spesific packages (nxcals-data-access-libs and nxcals_extraction_api_python3), hepak and the acc-py packages (pytimber, jpype1 and cmmnbuild_dep_manager)
* The two NXCALS and the acc-py packages are changing frequently, and a check for updates should be done daily.

The contact person in IT-DB-SAS for this stack is [Krishnan Raghavan](mailto:krishnan.raghavan@cern.ch).

### NXCALS packages

There are two main packages that need to be added to the stack for NXCALS:

1. `nxcals-data-access-libs`, a Java package that is provided pre-built as a tarball.
2. `nxcals_extraction_api_python3`, a Python3 package that is provided as a wheel (basically a zip file).

These packages are placed in two locations, one for dev builds (`/eos/project-n/nxcals/swan/nxcals_staging/`) and one for releases (`/eos/project-n/nxcals/swan/nxcals_pro/`)

### Acc-py packages

The acc-py packages can be downloaded from `https://acc-py-repo.cern.ch/repository/vr-py-releases/simple` with the command:
   `python3 -m pip download --index-url https://acc-py-repo.cern.ch/repository/vr-py-releases/simple --trusted-host acc-py-repo.cern.ch -d <install_dir> --no-deps <package>`

### HePak

The hePak package can be downloaded from its [git repo](https://gitlab.cern.ch/tytterda/HePak)

### Current state

The NXCALS and the acc-py packages has now been included in the stack. To check for updates, both the devbuilds and the releases will be run every night. To accomplish this, devnxcals has been added to our nightly builds, and NXCALS releases has their own Jenkins job: [lcg_nxcals_release_updater](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_nxcals_release_updater/). 
These packages are treated a bit differently then other packages. CMake will on toolchain level get hold on the latest version of these packages, and will then assign these versions dynamically.

HePak is using proprietary software, and can therefore only be made available for users at CERN. Since our cvmfs repo is open to the world, it was decided to publish the HePak package on the `/cvmfs/projects.cern.ch/` repo, which is only visible inside the CERN network. HePak is therefore not in the lcg stack.
Some info:

* Repo structure: `/cvmfs/projects.cern.ch/cryogenics/hepak/<LCG_VERSION> (with out LCG_ prefix)`
* Publisher node: sftnight@cvmfs-cryogenics
* There are currently not been created an automated way of installing and publishing the Hepak package. We are currently doing this by first sourcing a given view, then manually installing it with this command: `pip install  --prefix=/afs/cern.ch/user/s/sftnight/hepak/<LCG_VERSION> git+ssh://git@gitlab.cern.ch:7999/tytterda/HePak.git`. The cvcryogenics user does not have access to /afs/cern.ch/user/s/sftnight, so you will have to use scp to copy the package over to the desired location while in publish state. e.g.: `scp -r sftnight@lxplus.cern.ch:/afs/cern.ch/user/s/sftnight/hepak/100_nxcals/lib/python3.8/site-packages/hepak /cvmfs/projects.cern.ch/cryogenics/hepak/100_nxcals`

