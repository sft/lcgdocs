# CVMFS

The CernVM-File System (CernVM-FS) provides a scalable, reliable and low- maintenance software distribution service.

!!! note "Wiki"
    CernVM-FS has a lovely wiki, which can be found [here](https://cvmfs.readthedocs.io/en/stable/index.html). Further troubleshooting tips are located [here](https://cvmfs.readthedocs.io/en/stable/cpt-quickstart.html#troubleshooting).

## Access to the publishers

Access to the publishers is granted to the [lxcvmfs-sft](https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=lxcvmfs-sft) e-group. This e-group is administered by the [VOBox-VOC-SFT](https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=VOBox-VOC-SFT) e-group.

## Troubleshooting CVMFS on build nodes

!!! tip
    Commands below are generally related to the file system, and therefore require privileged access. Ensure that you are `root` before proceeding.

### "Transport endpoint is not connected"

The mount point of one or multiple repositories in `/cvmfs` might be broken. Identify the affected repositories by running `ls -l /cvmfs`. Repositories which are affected can have broken permissions, which can look like this:

```
d??????????  ? ?     ?        ?            ? sft-nightlies.cern.ch
```

Reset the affected mount points by running:

```
fusermount -u /cvmfs/<repository-name> && mount -a
```


## Gateways

* We have the following gateways for sft-test.cern.ch 

    * lxcvmfs170
    * lxcvmfs174
    * lxcvmfs175

* For sft-nightlies.cern.ch (and sft-nightlies-test.cern.ch maybe still?)

    * lxcvmfs146 
    * lxcvmfs171 
    * lxcvmfs172
    * lxcvmfs173

* gateway-cvmfs-sft01 is the gateway
