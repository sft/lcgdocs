# EOS

EOS is an open-source storage software solution to manage multi PB storage for the CERN Large Hadron Collidor LHC. 

EOS storage can be modified using `XRootD` tools and the `eos` command. These can be used from your account on `lxplus`. An environment variable has to be set in order to access the LCG project space:

```
export EOS_MGM_URL=root://eosproject-l.cern.ch
```

## Copy to EOS

After setup, go to the path where the files are, and copy them using `xrdcp`:

```
xrdcp -f <File_name> root://eosproject-l.cern.ch/path/in/eos/<File_name>
```

For further info: [EOS quick tutorial for beginners](https://cern.service-now.com/service-portal/article.do?n=KB0001998)


### Copy new source tarfiles to EOS

```
xrdcp -f <File_name> root://eosproject-l.cern.ch//eos/project/l/lcg/www/lcgpackages/tarFiles/sources/<File_name>
```

The common name convention for tarfiles is:

```
Name-1.0.0.tar
```

!!! important
    Beware that generators packages are all included in a special folder: *MCGenerators*, so the `xrdcp` command
    should be instead:

```
xrdcp -f <File_name> root://eosuser.cern.ch//eos/project/l/lcg/www/lcgpackages/tarFiles/sources/MCGenerators/<File_name>
```

## Check EOS quota
After setup:

```
export EOSHOME=/eos/project
eos quota
```

