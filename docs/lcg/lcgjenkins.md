# LCGjenkins

Repository containing all scripts used in jenkins.

## lcgjenkins/python ( currently in compilers branch )

This is meant to be a *lcg* python module with common tools.

Usage:
```python
import sys, os
sys.path.append("{}/../../python".format(os.path.dirname(os.path.realpath(__file__)))) ## Attach PYTHONPATH to the current env path - relative path to the edited file

from lcg.buildinfo import *
read_buildinfo_file(filename)
```

So far the only module is buildinfo to get the information from `.buildinfo_<pkgname>.txt`, but it can be enhanced by
functionality like: general tar operation on packages, information about release, processing the other text files with
build information.

