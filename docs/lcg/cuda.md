# Cuda

The Cuda toolkit is installed using pre-made binaries that can be downloaded from [CUDA toolkit
archive](https://developer.nvidia.com/cuda-toolkit-archive) for which you need to register as developer. The binaries
*run file(local)* have been copied to our internal EOS source area using the [lcg_urls_to_eos](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_urls_to_eos/) job by using the runfile link provided by the CUDA Toolkit Downloads page after selecting a target platform. In the Jenkins job, select "IS_INTERNAL" and remove the .run from the filename. 
The installation is a easy as just running the
downloaded self extracting script.


## Installing Package to CERN internal CVMFS

The Cuda, cudnn, and TensorRT packages have to be installed in the CERN internal projects CVMFS repository using the
publisher machine ``cvmfs-projectslcg``, which we can access the same way as our other sft publishers.

Simply create the correct folder for the `package/version/architecture` and inside it run (e.g.):

```
/cuda_12.3.2_545.23.08_linux.run --silent --override --toolkit --toolkitpath=$PWD --defaultroot=${PWD}
```

Or check the `lcgcmake/contrib/CMakeLists.txt` for the up-to-date command.

### Aarch64

We cannot run the aarch64 runfile on the cvmfs host, because it expects aarch64 architecture.
So connect to one of our arm machines and start docker with a dummy CVMFS structure just in case.
Then run the runfile 

```
docker run -u root -it -e SHELL -e WORKSPACE=/workspace  --cpus=10 --security-opt seccomp:unconfined -v /build:/build -v /ec/conf:/ec/conf --mount "type=volume,dst=/cvmfs"  --network host  gitlab-registry.cern.ch/sft/docker/alma9:aarch64 bash
mkdir -p /cvmfs/projects.cern.ch/lcg/releases/cuda/12.4/aarch64-linux
wget ....
cuda... --silent --override --toolkit --toolkitpath=$PWD --defaultroot=${PWD}
cd ..
tar cf cuda_aarch64.tar aarch64-linux
scp cuda_aarch64.tar /some/other/place
```
And extract the tarfile you just created on the projectslcg publisher in the same place.

## Install cuda to /cvmfs/sft.cern.ch/contrib

The install recipes essentially create links to those files on projects, except where their publication is allowed given
the respective software license agreement (SLA).

## Cuda

Carefully read the SLA and note the section about which files can be distributed. In the `lcgcmake` `contrib` folder,
create a file called `cuda_<SHORT_VERSION>_allowed_files.txt` with the list of files that can be distributed.

The [manual] installation to contrib is done with the following steps (being sftnight):
[For sft/spi users to become `sftnight` in lxplus,  execute the command: `~sftnight/public/lxplus`]

- set up the compiler with, for example, `source lcgcmake/jenkins/jk-setup.sh Release gcc11`. (Not strictly necessary, but gives tarball a more consistent name)
- add the new cuda version in toolchain file `heptools_contribpkgs.cmake`
- configure with the command with arguments `lcgcmake/bin/lcgcmake  configure -v contribpkgs --with-tarfiles --prefix /build/cuda/install`
    - Or some other `prefix` that is writable
- install locally with `lcgcmake/bin/lcgcmake install cuda`
- upload the binary tarfile with `lcgcmake/bin/lcgcmake upload tarfiles --release`
    - note the uploaded filename (especially its hash)
- in Jenkins
    - Mark the [cvmfs-sft](https://lcgapp-services.cern.ch/spi-jenkins/manage/computer/cvmfs%2Dsft/) node offline in jenkins
- login to CVMFS publisher: `ssh cvmfs-sft.cern.ch`
- in the CVMFS publisher node `cvmfs-sft`
    - Check the instructions in `/etc/motd`
    - Execute: `sudo -i -u cvsft`
    - Start a transaction in `sft.cern.ch` volume by executing the command: `cvmfs_server transaction sft.cern.ch`
    - Install the created tarfile with
        ```
        lcgcmake/cmake/scripts/install_binary_tarfile.py \
        --prefix /cvmfs/sft.cern.ch/lcg/releases \
        --nolink \
        --url http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/releases/<file>.tgz
        ```
    - Create links in `/cvmfs/sft.cern.ch/lcg/contrib/cuda`
    - Publish changes and close transaction: `cvmfs_server publish sft.cern.ch`
- in Jenkins
    - Bring the [cvmfs-sft](https://lcgapp-services.cern.ch/spi-jenkins/computer/cvmfs%2Dsft/) node online again

See the file `lcgcmake/contrib/cuda_install.sh` for details about how linking to the projects cvmfs repository is
done. Each file has to be linked explicitly so that it will also be reflected in the view.

## cudnn, TensorRT

For these packages we are only allowed to distribute shared libraries, everything else will be a link to
`/cvmfs/projects.cern.ch/lcg`. See the `lcgcmake/pyexternals/tensorrt_install.sh` or
`lcgcmake/externals/cudnn_install.sh` scripts for details.

Extract these packages the same way as cuda to the projects cvmfs repository, review their SLAs to make sure we are
still allowed to distributed shared libraries, and then installation via the LCG builds should be working.
