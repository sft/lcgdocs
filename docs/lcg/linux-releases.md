# How to build and install a LCG release for Linux

Building LCG releases for the supported Linux flavours is nowadays fully automatized and controlled by Jenkins and implemented through Jenkins pipelines. See the relevant scripts under [lcgcmake/jenkins](https://gitlab.cern.ch/sft/lcgcmake/-/tree/master/jenkins) and the Jenkins job [lcg_release_maxtrix](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_release_matrix/).


Still some manual steps are needed before a release should be made:

* Add the new RPM repo to the repo files under: `/eos/project/l/lcg/www/lcgpackages/lcg/etc/yum.repos.d/`
    * Both for the `lcg[789].repo` and `lcg[789]-debug.repo` files!

* Fork the `master` branch to `LCG_XYZ-patches` branch
* Adapt the gitlab CI file to use the new releae version instead of dev4
* In the folder `cmake/toolchain`  `git mv heptools-dev4.cmake heptools-XYZ.cmake` and similarly for `devswan` and `dev4cuda`. Change the files to point to the XYZ release file
* Fix the `garfield++` version and upload the related tarball
* [Configure gitlab](https://gitlab.cern.ch/sft/lcgcmake/-/settings/merge_requests) to automatically point `XYZ_featureBranches` to the `LCG_XYZ-patches` branch in the  `Merge request branch workflow` section.
