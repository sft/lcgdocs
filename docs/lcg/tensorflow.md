# Tensorflow

*The most recent version of tensorflow (1.12.0) has been installed using the already made wheel packages and not following the instructions building from sources*

### Installing already build binarires (on Linux and MacOS systems)
The [tensorflow installation page](https://www.tensorflow.org/install/pip) provides detailed instructions on the installation process using **pip**. At the end of the page there is a list of pre-made wheel files for different platforms and python versions. We have copied the relevant files to our EOS area without any change.
The installation becomes a simple pip install

    pip install --no-deps --prefix=<INSTALL_DIR> <SOURCE_DIR>/tensorflow${packsuffix}-${tensorflow_native_version}-${pyversion}-${pylibversion}-${os}_x86_64.whl


## Building tensorflow wheels from sources
Tensorflow is using *bazel* build system, that does not allow for an easy build.
In the following, the recipe for building version 1.14 w/ Python3.7 on CentOS7.

First install Python 3.7 and other dependencies

    yum install gcc gcc-c++ git
    yum install openssl-devel bzip2-devel libffi-devel
    cd /usr/src/
    curl -LJO https://www.python.org/ftp/python/3.7.4/Python-3.7.4.tgz
    tar xzf Python-3.7.4.tgz
    cd Python-3.7.4
    ./configure --enable-optimizations
    make altinstall
    /usr/local/bin/pip3.7 install six numpy wheel setuptools mock 'future>=0.17.1'
    /usr/local/bin/pip3.7 install keras_applications --no-deps
    /usr/local/bin/pip3.7 install keras_preprocessing --no-deps

Then create a work environment and set-up bazelisk, which is a wrapper around
bazel taking care of its proper installation

    export WORKDIR=<your work dir>
    mkdir $WORKDIR
    cd $WORKDIR
    curl -LJO https://github.com/bazelbuild/bazelisk/releases/download/v1.3.0/bazelisk-linux-amd64
    mv bazelisk-linux-amd64 bazel
    chmod +x bazel
    export PATH=$WORKDIR:$PATH

Now check out and set up Tensorflow

    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/python3.7
    git clone https://github.com/tensorflow/tensorflow.git
    cd tensorflow/
    git checkout v1.14.0

Patch the Tensorflow config to use the proper Bazel version

    echo 0.25.2 > .bazelversion

Patch the config to find the proper Python includes

    diff --git a/.bazelrc b/.bazelrc
    index d4d7ad6..35313b0 100644
    --- a/.bazelrc
    +++ b/.bazelrc
    @@ -103,8 +103,7 @@ build:c++1z --cxxopt=-stdlib=libc++
     # Default paths for TF_SYSTEM_LIBS
     build --define=PREFIX=/usr
     build --define=LIBDIR=$(PREFIX)/lib
    -build --define=INCLUDEDIR=$(PREFIX)/include
    -
    +build --define=INCLUDEDIR=$(PREFIX)/include:/usr/local/include/python3.7m/
     # Default options should come above this line

     # Options from ./configure


Configure Tensorflow and guide it to */usr/local/bin/python3.7*

    ./configure

Start the build and afterwards invoke the package building explicitly again

    bazel build --verbose_failures //tensorflow/tools/pip_package:build_pip_package
    bazel-bin/tensorflow/tools/pip_package/build_pip_package ./

The resulting wheel then can be copied to EOS.

## Building tensorflow wheels from sources with CUDA support

This explains how to build against CUDA 10.0. If building against newer versions, e.g. 10.2, patches may be necessary.

First download and install CUDA

    curl -LJO https://developer.download.nvidia.com/compute/cuda/repos/rhel7/x86_64/cuda-repo-rhel7-10.0.130-1.x86_64.rpm
    yum install cuda-libraries-dev-10-0

Then clear the bazel cache to prevent inconsistencies

    rm -rf /root/.cache/bazel

Set the environment and configure

    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/python3.7
    export PATH=/usr/local/cuda-10.0/bin/:/tensorflow:$PATH
    cd $WORKDIR/tensorflow
    ./configure

In *./configure* choose the following options

  * python in */usr/local/bin/python3.7*
  * enable *CUDA*
  * Use */usr/bin/gcc*, instead of */usr/gcc*. Even though they are the same, internal headers may not be found
  * search path for CUDA and related libraries is: /usr/local/cuda-10.0,/tensorflow/cuda,/usr/include,/usr/lib64

Then build it and create the wheel

    bazel build   --verbose_failures //tensorflow/tools/pip_package:build_pip_package
    bazel-bin/tensorflow/tools/pip_package/build_pip_package ./

Unfortunately the wheel name does not contain the required *tensorflow_gpu* name,
but just *tensorflow*. To fix this, patch the wheel

    mkdir wheelpatching_tmp
    cd wheelpatching_tmp
    unzip ../tensorflow-1.14.0-cp37-cp37m-linux_x86_64.whl
    mv tensorflow-1.14.0.data tensorflow_gpu-1.14.0.data
    mv tensorflow-1.14.0.dist-info tensorflow_gpu-1.14.0.dist-info
    sed -i 's/Name: tensorflow/Name: tensorflow-gpu/g' tensorflow-1.14.0.dist-info/METADATA
    sed -i 's/tensorflow-1.14.0/tensorflow_gpu-1.14.0/g' tensorflow-1.14.0.dist-info/RECORD
    zip -r tensorflow_gpu-1.14.0-cp37-cp37m-linux_x86_64.whl tensorflow_gpu-1.14.0.data  tensorflow_gpu-1.14.0.dist-info

the resulting wheel can be copied to EOS.

For future compilation attempts: Some bazel versions suffer from a problem in finding internal headers even after
pointing at */usr/bin/gcc*. In those cases all occurences of
*cxx_builtin_include_directories* in *third_party/gpus/crosstool/cc_toolchain_config.bzl.tpl*
need to be patched manually. It is not adviced to patch bazel itself,
as its own consistency checking needs to be disabled first.

## Building libtensorflow (C-API)
Recommended to use the node `lcgapp-centos7-physical2.cern.ch` for this operation.
- Build a docker image based on centos-7 and with all the needed packages (python3, bazel, gcc-7, etc.)
  ```
  git clone https://gitlab.cern.ch/sft/docker.git
  docker build -t tensorflow docker/special/tensorflow
  ```
- Run the image
  ```
  docker run -it tensorflow bash --login
  ```
- Instructions inside the image to build the C-API library (for tensorflow version 2.1). See [original instructions](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/tools/lib_package/README.md)
  ```
  git clone https://github.com/tensorflow/tensorflow.git
  cd tensorflow/
  git checkout v2.1.0

  export PYTHON_BIN_PATH=/usr/bin/python3
  ./configure
  export BAZEL_LINKLIBS=-l%:libstdc++.a
  bazel test --config opt //tensorflow/tools/lib_package:libtensorflow_test
  bazel build --config opt //tensorflow/tools/lib_package:libtensorflow
  ```
  Copy the resulting tarfile to EOS sources
